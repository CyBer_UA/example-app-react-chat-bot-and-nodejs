module.exports = [
    {
        model: "iPhone 7 128GB",
        brand: "Apple",
        price: 44.99,
        categories: ["Phones", "Tablets"]
    }, {
        model: "iPhone 7 32GB",
        brand: "Apple",
        price: 39.99,
        categories: ["Phones", "Tablets"]
    },
    {
        model: "iPhone 7 Plus 128GB",
        brand: "Apple",
        price: 49.99,
        categories: ["Phones", "Tablets"]
    },
    {
        model: "Galaxy S8 64GB",
        brand: "Samsung",
        price: 44.99,
        // storage: 128,
        categories: ["Phones", "Tablets"]
    },
    {
        model: "Galaxy S8+ 64GB",
        brand: "Samsung",
        price: 44.99,
        // storage: 128,
        categories: ["Phones", "Tablets"]
    },
    {
        model: "Drone BEBOP",
        brand: "Parrot",
        price: 44.99,
        categories: ["Drones"]
    },
    {
        model: "Drone BEBOP 2",
        brand: "Parrot",
        price: 59.99,
        categories: ["Drones"]
    },
    {
        model: "Vive",
        brand: "HTC",
        price: 59.99,
        categories: ["Gaming", "VR"]
    },
    {
        model: "Virtual Reality Glasses Rift VR",
        brand: "Oculus",
        price: 59.99,
        categories: ["Gaming", "VR"]
    },
    {
        model: "MacBook 12\" M-5Y31, 8GB RAM, 516GB",
        brand: "Apple",
        storage: 516,
        price: 59.99,
        categories: ["Computing"]
    },
    {
        model: "MacBook Air 11\" i7 2.2, 8GB RAM, 512GB",
        brand: "Apple",
        price: 64.99,
        categories: ["Computing"]
    },
    {
        model: "MacBook Air 13\" i5-5250U, 4GB RAM, 128GB",
        brand: "Apple",
        price: 69.99,
        categories: ["Computing"]
    },
    {
        model: "MacBook Pro 13\" i5-3210M, 4GB RAM, 500GB",
        brand: "Apple",
        price: 74.99,
        categories: ["Computing"]
    },
    {
        model: "Convertible Laptop Surface Book 512GB SSD Intel Core i7 16GB RAM dGPU",
        brand: "Microsoft",
        price: 59.99,
        categories: ["Computing"]
    },
    {
        model: "Convertible Laptop YOGA 300-11IBR 80M1004KGE",
        brand: "Lenovo",
        price: 59.99,
        categories: ["Computing"]
    },
    {
        model: "Watch 38mm",
        brand: "Apple",
        price: 39.99,
        categories: ["Wearables"]
    },
    {
        model: "Watch 42mm",
        brand: "Apple",
        price: 44.99,
        categories: ["Wearables"]
    },
    {
        model: "Watch Ambit 3",
        brand: "Suunto",
        price: 39.99,
        categories: ["Wearables"]
    },
    {
        model: "Watch V800",
        brand: "Polar",
        price: 39.99,
        categories: ["Wearables"]
    },
    {
        model: "Watch WI503Q-1LDBR0001",
        brand: "Asus",
        price: 44.99,
        categories: ["Wearables"]
    },
    {
        model: "Alexa Dot",
        brand: "Amazon",
        price: 44.99,
        categories: ["Smart Home"]
    },
    {
        model: "Alexa Echo",
        brand: "Amazon",
        price: 49.99,
        categories: ["Smart Home"]
    },
    {
        model: "Qbo Milk Master",
        brand: "Tchibo",
        price: 29.99,
        categories: ["Smart Home"]
    },
    {
        model: "Robotic Vacuum Cleaner POWERbot VR20J9020UR/EG",
        brand: "Samsung",
        price: 39.99,
        categories: ["Smart Home"]
    },
    {
        model: "Robotic Vacuum Cleaner POWERbot VR20J9259U/EG",
        brand: "Samsung",
        price: 39.99,
        categories: ["Smart Home"]
    }
];