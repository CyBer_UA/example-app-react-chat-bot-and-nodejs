const data = require("./data");
const spaceRegexp = /\s+/;
const _ = require("lodash");

class BotService {

    searchProduct(string) {
        if (_.isEmpty(string)) {
            throw new Error("Message can not be empty");
        }
        let products = this._searchProduct(string);
        products = products.map(item => ({model: item.model, price: item.price, id: item.id}));
        return {products};
    }

    _searchProduct(message) {
        message = message.toLowerCase().split(spaceRegexp);
        const categories = this._searchInCategories(message);
        if (categories.length > 0) {
            return categories;
        }
        const brands = this._searchInBrands(message);
        if (brands.length > 0) {
            return brands;
        }

        const names = this._searchInNames(message);
        if (names.length > 0) {
            return names;
        }

        return [];
    }

    _searchInCategories(message) {
        const categories = this._findInMap(message, data.categories);
        const filteredCategories = this._filterByMatchCount(categories, message);
        if(filteredCategories.length > 0) {
            return filteredCategories;
        }
        return categories;
    }

    _searchInBrands(message) {
        const brands = this._findInMap(message, data.brands);
        const filteredBrands = this._filterByMatchCount(brands, message);
        if(filteredBrands.length > 0) {
            return filteredBrands;
        }
        return brands;
    }

    _searchInNames(message) {
        return this._filterByMatchCount(data.items, message);
    }

    _filterByMatchCount(items, message) {
        return items.filter(item => {
            let entryCount = this._getMatchCount(message, item.model.toLowerCase());
            return entryCount > 0;
        });
    }

    _getMatchCount(message, string) {
        return message.reduce((sum, word) => {
            const isIncludes = word.length > 3 && string.includes(word);
            return isIncludes ? sum + 1 : sum;
        }, 0);
    }

    _findInMap(message, map) {
        let founded = [];
        message.forEach(word => {
            if (map.has(word.toLowerCase())) {
                founded = [...founded, ...map.get(word)];
            }
        });
        return founded;
    }
}


module.exports = new BotService();