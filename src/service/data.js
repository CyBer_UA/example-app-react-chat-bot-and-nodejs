const items = require("../../storage/dataStorage");
let categories = new Map();
let brands = new Map();

function addToMap(item, key, map) {
    key = key.toLowerCase();
    if (map.has(key)) {
        map.get(key).push(item)
    } else {
        map.set(key, [item]);
    }
}

items.forEach(item => {
    addToMap(item, item.brand, brands);
    item.categories.forEach(category => addToMap(item, category, categories));
});


module.exports = {
    brands, categories, items
};