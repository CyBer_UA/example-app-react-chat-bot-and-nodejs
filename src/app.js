const Koa = require('koa');
const app = new Koa();
const router = require("./router/charRouter");
const bodyParser = require('koa-bodyparser');
const mount = require('koa-mount');
const serve = require('koa-static');
const path = require('path');
const views = require('koa-render-view');

const viewPath = path.join(__dirname, '../views');
app.use(views(viewPath));
app.use(mount('/static/bot/', serve(path.join(__dirname, '../node_modules/react-chat-bot-example/dist'))));
app.use(bodyParser());
app.use(router);
app.listen(9010, () => console.log("Server started on localhost:9010"));