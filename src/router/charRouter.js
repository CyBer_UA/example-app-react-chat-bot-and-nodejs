const Router = require('koa-router');
const botService = require("../service/searchService");
const router = new Router();

router.post("/search/product/", ctx => {
    let {message} = ctx.request.body;
    ctx.body = botService.searchProduct(message);
});
router.get("/", async ctx => {
    await ctx.render("bot")
});

module.exports = router.routes();